﻿namespace TicTacToeServer.BusinessEntities
{
    public class Answer
    {
        public Move ComputerMove { get; set; }
        public GameStatus Status { get; set; }
    }
}
