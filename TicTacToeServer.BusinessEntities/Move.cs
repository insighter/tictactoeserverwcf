﻿namespace TicTacToeServer.BusinessEntities
{
    public class Move
    {
        public int X { get; set; }

        public int Y { get; set; }
    }
}
