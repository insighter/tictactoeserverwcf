﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TicTacToeServer.BusinessEntities
{
    public enum GameStatus
    {
        InProcess = 0,
        UserWon = 1,
        ComputerWon = 2,
        Draw = 3
    }
}