﻿using System;
using System.Text;

namespace TicTacToeServer.BusinessEntities
{    
    public class Game
    {     
        public Guid ID { get; set; }

        public GameStatus Status { get; set; }

        public Cell[] Board { get; set; }

        public Guid UserID { get; set; }
    }
}
