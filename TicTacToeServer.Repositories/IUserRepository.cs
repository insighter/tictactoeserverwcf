﻿using System;
using System.Collections.Generic;
using TicTacToeServer.BusinessEntities;

namespace TicTacToeServer.Repositories
{
    public interface IUserRepository
    {
        void Create(User user);

        void Save(User user);

        User GetByID(Guid id);

        List<User> GetAll();

        void Delete(Guid id);
    }
}
