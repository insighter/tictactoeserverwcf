﻿using System;

namespace TicTacToeServer.Repositories.DBEntities
{
    public class Game
    {    
        public Guid ID { get; set; }

        public int Status { get; set; }

        public string Board { get; set; }                       

        public Guid UserID { get; set; }
    }
}
