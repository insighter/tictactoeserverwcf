﻿using System;
using System.Collections.Generic;

namespace TicTacToeServer.Repositories.DBEntities
{
    public class User
    {   
        public Guid ID { get; set; }

        public List<Game> Games { get; set; }
    }
}
