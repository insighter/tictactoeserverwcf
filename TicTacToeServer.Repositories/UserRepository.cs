﻿using System;
using System.Linq;
using System.Data.Entity;
using System.Collections.Generic;
using TicTacToeServer.BusinessEntities;

namespace TicTacToeServer.Repositories
{
    public class UserRepository : IUserRepository
    {
        private TicTacToeContext _context;

        public UserRepository(TicTacToeContext context)
        {
            _context = context;
        }

        public void Create(User user)
        {               
            var userEntity = new DBEntities.User
            {
                ID = user.ID
            };
      
            if (_context.Users.Where(u => u.ID == user.ID).Any())
            {
                throw new ArgumentException("User ID is already used");
            }

            _context.Users.Add(userEntity);

            _context.SaveChanges();
        }

        public void Save(User user)
        {
            var userEntity = _context.Users.Where(u => u.ID == user.ID).SingleOrDefault();
            if (userEntity == null)
            {
                throw new ArgumentException("Unknown user ID");
            }

            _context.SaveChanges();
        }

        public User GetByID(Guid id)
        {
            var userEntity = _context.Users.Where(u => u.ID == id).SingleOrDefault();

            if (userEntity == null)
            {
                return null;
            }

            return new User
            {
                ID = userEntity.ID
            };
        }

        public List<User> GetAll()
        {  
            var users = _context.Users.Select(u => new User { ID = u.ID }).ToList();
            return users;
        }

        public void Delete(Guid id)
        {
            var userEntity = _context.Users.Where(u => u.ID == id).SingleOrDefault();
            if (userEntity == null)
            {
                throw new ArgumentException("Unknown user ID");
            }

            _context.Users.Remove(userEntity);
            _context.SaveChanges();
        }
    }
}
