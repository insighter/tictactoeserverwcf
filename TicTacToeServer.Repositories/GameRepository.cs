﻿using System;
using System.Text;
using System.Linq;
using System.Collections.Generic;
using TicTacToeServer.BusinessEntities;

namespace TicTacToeServer.Repositories
{
    public class GameRepository : IGameRepository
    {
        private TicTacToeContext _context;

        public GameRepository(TicTacToeContext context)
        {
            _context = context;
        }

        public void Create(Game game)
        {           
            var user = _context.Users.Where(u => u.ID == game.UserID).SingleOrDefault();
            if (user == null)
            {
                throw new ArgumentException("Game user ID is unknown");
            }

            var gameEntity = new DBEntities.Game
            {
                ID = game.ID,
                Board = ConvertBoardToString(game.Board),
                Status = (int)game.Status,                
                UserID = game.UserID
            };

            _context.Games.Add(gameEntity);

            _context.SaveChanges();
            
        }

        public void Save(Game game)
        {     
            var gameEntity = _context.Games.Where(g => g.ID == game.ID).SingleOrDefault();
            if (gameEntity == null)
            {
                throw new ArgumentException("Unknown game ID");
            }

            gameEntity.Status = (int)game.Status;
            gameEntity.Board = ConvertBoardToString(game.Board);            
            gameEntity.UserID = game.UserID;

            _context.SaveChanges();            
        }

        public Game GetByID(Guid id)
        {          
            var gameEntity = _context.Games.Where(g => g.ID == id).SingleOrDefault();
            if (gameEntity == null)
            {
                return null;
            }

            return new Game
            {
                ID = gameEntity.ID,
                Status = (GameStatus)gameEntity.Status,
                Board = ConvertStringToBoard(gameEntity.Board),                
                UserID = gameEntity.UserID
            };            
        }

        public List<Game> GetByUserID(Guid userID)
        {       
            var gameEntities = _context.Games.Where(g => g.UserID == userID).ToList();

            var games = gameEntities.Select(x => new Game
            {
                ID = x.ID,
                Status = (GameStatus)x.Status,
                Board = ConvertStringToBoard(x.Board),                
                UserID = x.UserID
            }).ToList();

            return games;            
        }   

        public void Delete(Guid id)
        {
            var gameEntity = _context.Games.Where(g => g.ID == id).SingleOrDefault();
            if (gameEntity == null)
            {
                throw new ArgumentException("Unknown game ID");
            }

            _context.Games.Remove(gameEntity);
            _context.SaveChanges();
        }

        private static string ConvertBoardToString(Cell[] board)
        {
            var builder = new StringBuilder();
            for (var i = 0; i < board.Length; i++)
            {
                builder.Append((int)board[i]);
            }

            return builder.ToString();
        }

        private static Cell[] ConvertStringToBoard(string s)
        {
            var board = new Cell[s.Length];
            for (var i = 0; i < s.Length; i++)
            {
                board[i] = (Cell)Enum.Parse(typeof(Cell), s[i].ToString());    
            }

            return board;
        }
    }
}
