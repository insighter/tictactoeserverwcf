﻿using System;
using System.Collections.Generic;
using TicTacToeServer.BusinessEntities;

namespace TicTacToeServer.Repositories
{
    public interface IGameRepository
    {
        void Create(Game game);

        void Save(Game game);

        Game GetByID(Guid id);

        List<Game> GetByUserID(Guid userID);
    }
}
