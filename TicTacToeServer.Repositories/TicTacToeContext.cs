﻿using System.Data.Entity;
using System.ComponentModel.DataAnnotations.Schema;
using TicTacToeServer.Repositories.DBEntities;

namespace TicTacToeServer.Repositories
{
    public class TicTacToeContext : DbContext
    {
        public TicTacToeContext() : base("TicTacToe")
        {
        }
     
        public DbSet<Game> Games { get; set; }

        public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Game>()
                .Property(c => c.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None)
                .HasColumnName("id");

            modelBuilder.Entity<Game>()
                .Property(c => c.Status)
                .HasColumnName("status");

            modelBuilder.Entity<Game>()
                .Property(c => c.Board)
                .HasColumnName("board")
                .HasColumnType("char")
                .HasMaxLength(9)
                .IsFixedLength();
        
            modelBuilder.Entity<Game>()
                .Property(c => c.UserID)
                .HasColumnName("userID");

            modelBuilder.Entity<User>()
                .Property(c => c.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None)
                .HasColumnName("id");
        }
    }
}
