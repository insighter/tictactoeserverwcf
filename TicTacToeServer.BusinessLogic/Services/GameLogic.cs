﻿using System;
using System.Collections.Generic;
using System.Linq;
using TicTacToeServer.BusinessEntities;
using TicTacToeServer.Repositories;
using TicTacToeServer.BusinessLogic.Exceptions;

namespace TicTacToeServer.BusinessLogic.Services
{
    public class GameLogic : IGameLogic 
    {
        private IUserRepository _userRepository;
        private IGameRepository _gameRepository;

        private const int Xmax = 3;
        private const int Ymax = 3;

        private static int[,] _winPositions;

        private static Cell _userMark = Cell.X;
        private static Cell _computerMark = Cell.O;

        public GameLogic(IUserRepository userRepository, IGameRepository gameRepository)
        {
            _userRepository = userRepository;
            _gameRepository = gameRepository;

            _winPositions = new int[8, Xmax]
            {
                { 0, 1, 2 }, { 3, 4, 5 }, { 6, 7, 8 },
                { 0, 3, 6 }, { 1, 4, 7 }, { 2, 5, 8 },
                { 0, 4, 8 }, { 2, 4, 6 }
            };
        }

        public Guid StartGame(Guid userID)
        {
            var user = _userRepository.GetByID(userID);
            if (user == null)
            {
                _userRepository.Create(new User { ID = userID });
            }            

            var game = new Game
            {                
                ID = Guid.NewGuid(),
                Board = CreateEmptyBoard(),
                Status = GameStatus.InProcess,
                UserID = userID
            };
            _gameRepository.Create(game);

            return game.ID;
        }

        public Answer MakeMove(Guid gameID, Move userMove)
        {
            var game = _gameRepository.GetByID(gameID);
            ThrowIfUnknownGameID(game);
            ThrowIfGameEnded(game);
            ThrowIfMoveNull(userMove);
            ThrowIfXOutOfRange(userMove);
            ThrowIfYOutOfRange(userMove);

            var moveCellNumber = ToCellNumber(userMove);
            ThrowIfCellIsNotEmpty(userMove, game, moveCellNumber);

            game.Board[moveCellNumber] = _userMark;
                      
            Move computerMove = null;
            if (IsWinPosition(game.Board, _userMark))
            {               
                computerMove = new Move { X = -1, Y = -1 };
                game.Status = GameStatus.UserWon;
            }

            if (game.Status == GameStatus.InProcess && GameEnds(game.Board))
            {        
                computerMove = new Move { X = -1, Y = -1 };
                game.Status = GameStatus.Draw;
            }

            if (game.Status == GameStatus.InProcess)
            {
                computerMove = ProcessComputerMove(game);
            }

            var answer = new Answer
            {
                ComputerMove = computerMove,
                Status = game.Status
            };       

            _gameRepository.Save(game);

            return answer;
        }

        #region Game logic helpers

        private Move ProcessComputerMove(Game game)
        {
            Move answerMove;
            var choice = 0;
            MiniMax(game.Board, _computerMark, ref choice);
            game.Board = MakeMove(game.Board, _computerMark, choice);

            if (IsWinPosition(game.Board, _computerMark))
            {
                game.Status = GameStatus.ComputerWon;
            }

            if (game.Status == GameStatus.InProcess && GameEnds(game.Board))
            {
                game.Status = GameStatus.Draw;
            }

            var coordinates = ToCoordinates(choice);

            answerMove = new Move { X = coordinates.Item1, Y = coordinates.Item2 };
            return answerMove;
        }

        private static int MiniMax(Cell[] initialBoard, Cell mark, ref int choice)
        {
            var board = CloneBoard(initialBoard);
            var score = GetScore(board, _computerMark);

            if (score != 0)
            {
                return score;
            }
            else if (GameEnds(board))
            {
                return 0;   
            }

            var scores = new List<int>();
            var moves = new List<int>();

            for (var i = 0; i < board.Length; i++)
            {
                if (board[i] == Cell.Empty)
                {
                    scores.Add(MiniMax(MakeMove(board, mark, i), GetOppositeMark(mark), ref choice));
                    moves.Add(i);
                }
            }

            if (mark == _computerMark)
            {
                int maxScoreIndex = scores.IndexOf(scores.Max());
                choice = moves[maxScoreIndex];
                return scores.Max();
            }
            else
            {
                int minScoreIndex = scores.IndexOf(scores.Min());
                choice = moves[minScoreIndex];
                return scores.Min();
            }
        }

        private static int GetScore(Cell[] board, Cell mark)
        {
            var score = 0;

            if (IsWinPosition(board, mark))
            {
                score = 10;
            }
            else if (IsWinPosition(board, GetOppositeMark(mark)))
            {
                score = -10;
            }

            return score;
        }

        private static bool IsWinPosition(Cell[] board, Cell mark)
        {
            var win = false;

            for (var i = 0; i < Xmax * Ymax - 1; i++)
            {
                if (board[_winPositions[i, 0]] == mark &&
                    board[_winPositions[i, 1]] == mark &&
                    board[_winPositions[i, 2]] == mark)
                {
                    win = true;
                    break;
                }
            }

            return win;
        }

        private static bool GameEnds(Cell[] board)
        {
            var end = true;

            foreach (var cell in board)
            {
                if (cell == Cell.Empty)
                {
                    end = false;
                    break;
                }
            }

            return end;
        }

        private static Cell GetOppositeMark(Cell mark)
        {
            if (mark == Cell.Empty)
            {
                throw new InternalLogicException("Empty cell mark is not allowed for this operation");
            }
            return mark == Cell.O ? Cell.X : Cell.O;
        }

        private static Cell[] MakeMove(Cell[] board, Cell mark, int cellNumber)
        {
            var clonedBoard = CloneBoard(board);
            clonedBoard[cellNumber] = mark;
            return clonedBoard;
        }

        private static Cell[] CloneBoard(Cell[] board)
        {
            var clonedBoard = new Cell[board.Length];
            for (var i = 0; i < board.Length; i++)
            {
                clonedBoard[i] = board[i];
            }
            return clonedBoard;
        }

        private int ToCellNumber(Move move)
        {
            return move.X + move.Y * Ymax;
        }

        private Tuple<int, int> ToCoordinates(int cellNumber)
        {
            var y = cellNumber / Ymax;
            var x = cellNumber - y * Ymax;
            return Tuple.Create(x, y);
        }

        private Cell[] CreateEmptyBoard()
        {
            var length = Xmax * Ymax;
            var emptyBoard = new Cell[length];
            Array.Clear(emptyBoard, 0, length);            

            return emptyBoard;
        }

        #endregion

        #region Check arguments

        private static void ThrowIfCellIsNotEmpty(Move userMove, Game game, int moveCellNumber)
        {
            if (game.Board[moveCellNumber] != Cell.Empty)
            {
                throw new InvalidMoveException($"Invalid move: cell [{userMove.X}, {userMove.Y}] is not empty");
            }
        }

        private static void ThrowIfUnknownGameID(Game game)
        {
            if (game == null)
            {
                throw new UnknownGameException();
            }
        }

        private static void ThrowIfMoveNull(Move move)
        {
            if (move == null)
            {
                throw new ArgumentNullException("move");
            }
        }

        private static void ThrowIfYOutOfRange(Move userMove)
        {
            if (userMove.Y >= Ymax || userMove.Y < 0)
            {
                throw new ArgumentOutOfRangeException("Y coordinate is out of range");
            }
        }

        private static void ThrowIfXOutOfRange(Move userMove)
        {
            if (userMove.X >= Xmax || userMove.X < 0)
            {
                throw new ArgumentOutOfRangeException("X coordinate is out of range");
            }
        }

        private static void ThrowIfGameEnded(Game game)
        {
            if (game.Status != GameStatus.InProcess)
            {
                throw new ArgumentException("Game ended");
            }
        }

        #endregion
    }
}
