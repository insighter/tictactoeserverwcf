﻿using System;
using TicTacToeServer.BusinessEntities;

namespace TicTacToeServer.BusinessLogic.Services
{
    public interface IGameLogic
    {
        Guid StartGame(Guid userID);

        Answer MakeMove(Guid gameID, Move userMove);
    }
}
