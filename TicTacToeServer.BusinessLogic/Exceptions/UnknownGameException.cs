﻿using System;
using System.Runtime.Serialization;

namespace TicTacToeServer.BusinessLogic.Exceptions
{
    [Serializable]
    public class UnknownGameException : Exception, ISerializable
    {
        public UnknownGameException() { }

        public UnknownGameException(string message) : base(message) { }

        public UnknownGameException(string message, Exception inner) : base(message, inner) { }

        protected UnknownGameException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }
}
