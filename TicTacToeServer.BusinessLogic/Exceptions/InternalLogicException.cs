﻿using System;
using System.Runtime.Serialization;

namespace TicTacToeServer.BusinessLogic.Exceptions
{
    [Serializable]
    public class InternalLogicException : Exception, ISerializable
    {
        public InternalLogicException() { }

        public InternalLogicException(string message) : base(message) { }

        public InternalLogicException(string message, Exception inner) : base(message, inner) { }

        protected InternalLogicException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }    
}
